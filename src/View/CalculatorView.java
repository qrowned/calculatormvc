package View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public final class CalculatorView extends JFrame {

    private final JTextField num1Field = new JTextField(10);
    private final JTextField num2Field = new JTextField(10);
    private final JTextField sumField = new JTextField(10);
    private final JButton calculateButton = new JButton("Calculate");

    public CalculatorView() {
        JPanel panel = new JPanel(new GridLayout(4, 2));
        panel.add(new JLabel("Number 1: "));
        panel.add(this.num1Field);
        panel.add(new JLabel("Number 2: "));
        panel.add(this.num2Field);
        panel.add(new JLabel("Result: "));
        this.sumField.setEditable(false);
        panel.add(this.sumField);
        panel.add(this.calculateButton);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(300, 200);
        this.setContentPane(panel);
        this.setTitle("Calculator");
    }

    public int getNum1() {
        try {
            return Integer.parseInt(this.num1Field.getText());
        } catch (NumberFormatException ignored) {
            return 0;
        }
    }

    public int getNum2() {
        try {
            return Integer.parseInt(this.num2Field.getText());
        } catch (NumberFormatException ignored) {
            return 0;
        }
    }

    public void setSum(int sum) {
        this.sumField.setText(Integer.toString(sum));
    }

    public void addCalculateListener(ActionListener listener) {
        this.calculateButton.addActionListener(listener);
    }

}

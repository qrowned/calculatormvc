package Controller;

import Model.CalculatorModel;
import View.CalculatorView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public final class CalculatorController {

    private final CalculatorView view;
    private final CalculatorModel model;

    public CalculatorController(CalculatorView view, CalculatorModel model) {
        this.view = view;
        this.model = model;

        view.addCalculateListener(new CalculateListener());
    }

    class CalculateListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int num1 = view.getNum1();
            int num2 = view.getNum2();

            model.setNum1(num1);
            model.setNum2(num2);

            int sum = model.getSum();

            view.setSum(sum);
        }
    }

}
